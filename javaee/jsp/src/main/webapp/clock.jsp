<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <title>JSP Clock</title>
    </head>
    <body>
        <div align="center">
            <br/>
            Hello there!
            <br/><br/>
            It's been <%= System.currentTimeMillis() %> milliseconds since midnight, January 1st 1970.
            <br/><br/>
            In other words, it's
            <%
                Date now = new Date();
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEEEEEE"); // Это название дня недели...
                String today = simpleDateFormat.format(now);

                // Java EE 7 The Big Picture, Chapter 3 Dynamic Web Pages: JSP:
                // In this line of code, out refers to the output of the underlying HTTP response that carries the
                // content back to the calling client, and is a type of java.io.Writer from the JSP API, specifically,
                //     javax.servlet.jsp.JspWriter . Notice that this variable has not been declared anywhere
                // else in the JSP: that is because the Java environment within JSP expressions comes preconfigured
                // with a number of objects called implicit objects. Other examples include the session variable
                // (the current HttpSession ) and the response variable (the current HttpServletResponse ).
                out.println(today.trim());
            %>.
        </div>
    </body>
</html>
