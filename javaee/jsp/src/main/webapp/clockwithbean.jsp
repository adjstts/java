<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>JSP Clock</title>
    </head>
    <body>
        <jsp:useBean id="clockBean" class="corp.adjstts.jsp.clock.ClockBean"/>
        <div align="center">
            <br/>
            Hello there!
            <br/><br/>
            It's been <jsp:getProperty name="clockBean" property="currentTimeSinceEpoch"/> milliseconds since midnight,
            January 1st 1970.
            <br/><br/>
            In other words, it's <jsp:getProperty name="clockBean" property="readableDate"/>
        </div>
    </body>
</html>
