<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- смотреть pom.xml -->
<html>
    <head>
        <title>For each (Standard Tag Library)</title>
    </head>
    <body>
        <jsp:useBean id="myWallet" class="corp.adjstts.jsp.foreach.MyWallet"/>

        Odd Numbers up to 20:
        <c:forEach var="i" begin="1" end="20" step="2">
            ${i}&nbsp;
        </c:forEach>
        <p/>
        Coins I have:
        <c:forEach var="coin" items="${myWallet.coins}">
            ${coin}&nbsp;
        </c:forEach>
        <p/>
        Notes I have:
        <c:forEach var="note" items="${myWallet.notes}">
            ${note}&nbsp;
        </c:forEach>
        <p/>
        Receipts:
        <c:forEach var="receipt" items="${myWallet.receipts}">
            ${receipt}&nbsp;
        </c:forEach>
    </body>
</html>
