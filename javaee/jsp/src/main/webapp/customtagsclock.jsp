<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="custom" uri="http://my/custom/tags" %>
<%--Можно было написать как написано в книге:--%>
<%--<%@ taglib prefix="custom" uri="WEB-INF/custom_tags.tld" %>--%>
<%--И не пришлось бы заводить uri для кастомных тегов--%>
<%--Но IDEA не умеет подсвечивать теги, если указан относительный путь до них, а не uri--%>
<html>
    <head>
        <title>JSP Clock with custom tag</title>
    </head>
    <body>
        <div align="center">
            <br/>
            Hello there!
            <br/><br/>
            It's been <custom:time-since-epoch/> milliseconds since midnight,
            January 1st 1970.
            <br/><br/>
            In other words, it's <custom:date dateFormat="h:mm a, zzzz"/>
        </div>
    </body>
</html>
