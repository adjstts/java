<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <title>JSP Clock</title>
    </head>
    <body>
        <jsp:useBean id="clockBean" class="corp.adjstts.jsp.clock.ConfigurableClockBean"/>
        <div align="center">
            <br/>
            Hello there!

            <br/><br/>
            It's been <jsp:getProperty name="clockBean" property="currentTimeSinceEpoch"/> milliseconds since midnight,
            January 1st 1970.

            <br/><br/>
            In other words, it's <jsp:getProperty name="clockBean" property="readableDate"/>

            <br/><br/>
            <jsp:setProperty name="clockBean" property="dateFormat" value="MMMMMMMMM"/>
            Or in other words, it’s the month of <jsp:getProperty name="clockBean" property="readableDate"/>

            <jsp:setProperty name="clockBean" property="dateFormat" value="YYYY"/>
            in <jsp:getProperty name="clockBean" property="readableDate"/>
        </div>
    </body>
</html>
