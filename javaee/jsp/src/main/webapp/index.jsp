<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> <!-- смотреть pom.xml -->
<html>
    <head>
        <title>Sample JSP Page</title>
    </head>
    <body>
        <jsp:useBean id="sampleBean" scope="application" class="corp.adjstts.jsp.SampleBean"/>
        <p>Hello from a JSP page!</p>
        <ul>
            <c:forEach var="item" items="${sampleBean.items}">
                <li>${item}</li>
            </c:forEach>
        </ul>
    </body>
</html>
