package corp.adjstts.jsp.foreach;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyWallet
{
    private String[] coins = {"1¢", "1¢", "5¢", "25¢", "25¢"};
    private String[] notes = {"$1", "$1", "$1", "$5", "$10", "$10", "$20"};
    private String[] receipts = {"gas - $42.50", "groceries - $35.26", "bookstore - $12.99"};

    public String[] getCoins() {
        return coins;
    }

    public List<String> getNotes() {
        return Arrays.asList(notes);
    }

    public Set getReceipts() {
        return new HashSet<>(Arrays.asList(receipts));
    }
}
