package corp.adjstts.jsp.customTagLibrary;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyDateTag extends SimpleTagSupport {
    private String dateFormat;

    // Это свойство является аттрибутом создаваемого тега.
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    @Override
    public void doTag() throws IOException {
        JspWriter out = getJspContext().getOut();
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        String today = simpleDateFormat.format(now);

        out.println(today);
    }
}
