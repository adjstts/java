package corp.adjstts.jsp;

import java.util.ArrayList;
import java.util.List;

public class SampleBean {
    private List<String> items;

    public SampleBean() {
        items = new ArrayList<>();
        items.add("First Item");
        items.add("Second Item");
        items.add("Third Item");
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }
}
