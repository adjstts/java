package corp.adjstts.jsp.clock;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ConfigurableClockBean {
    private String dateFormat = "EEEEEEEE";

    // В JSP к этому свойству можно обращаться так: clockBean.currentTimeSinceEpoch
    public long getCurrentTimeSinceEpoch() {
        return System.currentTimeMillis();
    }

    // В JSP к этому свойству можно обращаться так: clockBean.dateFormat
    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    // В JSP к этому свойству можно обращаться так: clockBean.readableDate
    public String getReadableDate() {
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        String today = simpleDateFormat.format(now);

        return today;
    }
}
