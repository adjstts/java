package corp.adjstts.jsp.clock;

public class ClockBean {
    // Было три варианта:
    // 1. Копипастить код из ClockBean в ConfigurableClockBean.
    // 2. Понаследовать ConfigurableClockBean от ClockBean.
    // 3. Использовать композицию объектов.
    // Здесь реализован третий вариант, потому что мне захотелось попрактиковаться в использовании композиции объектов.
    private ConfigurableClockBean configurableClockBean;

    public ClockBean()
    {
        configurableClockBean = new ConfigurableClockBean();
    }

    // В JSP к этому свойству можно обращаться так: clockBean.currentTimeSinceEpoch
    public long getCurrentTimeSinceEpoch() {
        return configurableClockBean.getCurrentTimeSinceEpoch();
    }

    // В JSP к этому свойству можно обращаться так: clockBean.readableDate
    public String getReadableDate() {
        return configurableClockBean.getReadableDate();
    }
}
