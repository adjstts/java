package adjstts.java.resteasyjaxrs.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/sync")
public class SyncResource {
    @GET
    public String get() {
        return "sync GET successful";
    }
}
