package adjstts.java.resteasyjaxrs.server;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

@Path("/async")
public class AsyncResource {
    // JAX-RS 2.0 Specification
    // Chapter 8. Asynchronous Processing
    // https://download.oracle.com/otn-pub/jcp/jaxrs-2_0-fr-eval-spec/jsr339-jaxrs-2.0-final-spec.pdf?AuthParam=1550335090_004fe1d3c04bd261299243322ce16b97#chapter.8
    @GET
    public void get(@Suspended AsyncResponse asyncResponse) {
        asyncResponse.resume("async GET successful");
    }
}
