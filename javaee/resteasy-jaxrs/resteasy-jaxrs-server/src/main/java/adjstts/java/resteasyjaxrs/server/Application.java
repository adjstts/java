package adjstts.java.resteasyjaxrs.server;

import javax.ws.rs.ApplicationPath;

// В спеке JAX-RS где-то говорится, что REST-сервисы можно писать без дескриптора,
// контейнер просто ищет в архиве класс, наследующий javax.ws.rs.core.Application.
@ApplicationPath("/rest")
public class Application extends javax.ws.rs.core.Application {
}
