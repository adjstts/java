package adjstts.java.resteasyjaxrs.client;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * RESTEasy 3.0.6.Final Reference Guide
 * 48.1. JAX-RS 2.0 Client API
 * https://docs.jboss.org/resteasy/docs/3.0.6.Final/userguide/html_single/index.html#d4e2098
 */
public class ClientTest {
    private Client client;

    @Before
    public void setUp() {
        client = ClientBuilder.newClient();
    }

    @Test
    public void testSyncResource() {
        WebTarget target = client.target(TestURLs.BASE + TestURLs.SYNC);

        Response response = target.request().get();
        String value = response.readEntity(String.class);
        response.close();

        Assert.assertEquals("sync GET successful", value);
    }

    @Test
    public void testAsyncResource() {
        WebTarget target = client.target(TestURLs.BASE + TestURLs.ASYNC);

        Response response = target.request().get();
        String value = response.readEntity(String.class);
        response.close();

        Assert.assertEquals("async GET successful", value);
    }
}
