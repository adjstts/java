package adjstts.java.resteasyjaxrs.client;

class TestURLs {
    static final String BASE =
        "http://localhost:8080/resteasy-jaxrs-server-1.0-SNAPSHOT/rest";

    static final String SYNC = "/sync";

    static final String ASYNC = "/async";
}
