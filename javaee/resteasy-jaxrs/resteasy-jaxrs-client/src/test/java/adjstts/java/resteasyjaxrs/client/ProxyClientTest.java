package adjstts.java.resteasyjaxrs.client;

import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * RESTEasy 3.0.6.Final Reference Guide
 * 48.2. Resteasy Proxy Framework
 * https://docs.jboss.org/resteasy/docs/3.0.6.Final/userguide/html_single/index.html#d4e2106
 *
 * Resteasy has a simple API based on Apache HttpClient. You generate a proxy
 * then you can invoke methods on the proxy. The invoked method gets translated
 * to an HTTP request based on how you annotated the method and posted to the
 * server.
 *
 * The framework also supports the JAX-RS locator pattern, but on the client
 * side. So, if you have a method annotated only with @Path, that proxy method
 * will return a new proxy of the interface returned by that method.
 *
 * It is generally possible to share an interface between the client and server.
 * In this scenario, you just have your JAX-RS services implement an annotated
 * interface and then reuse that same interface to create client proxies to
 * invoke on the client-side.
 * (Так, в Krypton JavaFX-клиент и веб-тесты ипсользуют прокси тех же
 * интерфейсов, которые реализуют REST-сервисы на сервере).
 */
public class ProxyClientTest {
    // Отличие ResteasyClient от Client в том, что ResteasyClient#target
    // возвращает сразу ResteasyWebTarget. Метод Client#target возвращает
    // WebTarget, который необходимо самостоятельно кастить в
    // ResteasyWebTarget. ResteasyWebTarget нам нужен потому, что он
    // содержит метод .proxy(), создающий клиентские прокси для тестируемых
    // здесь REST-сервисов.
    private ResteasyWebTarget target;

    @Before
    public void setUp() {
        // Важно, что используемая здесь версия RESTEasy - 3.0.19.Final, как и в
        // Krypton. Потому что, например, в версии 4.0.0.Beta8, класс
        // ResteasyClientBuilder уже абстрактный и его нельзя инстанциировать,
        // как это делается ниже:
        target = new ResteasyClientBuilder().build().target(TestURLs.BASE);
    }

    @Test
    public void testSyncResource() {
        SyncResource resource = target.proxy(SyncResource.class);

        String value = resource.get();

        Assert.assertEquals("sync GET successful", value);
    }

    @Test
    public void testAsyncResource() {
        AsyncResource resource = target.proxy(AsyncResource.class);

        String value = resource.get();

        Assert.assertEquals("async GET successful", value);
    }

    // Два интерфейса ниже можно было даже объединить в один, поставив каждому
    // методу соответствующий path. Необходимости писать интерфейсы, полностью
    // совпадающие с интерфейсами вызываемых веб-сервисов, нет.

    @Path(TestURLs.SYNC)
    private interface SyncResource {
        @GET
        String get();
    }

    @Path(TestURLs.ASYNC)
    private interface AsyncResource {
        // На сервере этот метод REST-сервиса асинхронный, т.е. один из его
        // параметров - @Suspended AsyncResponse. Так как он инжектится в метод
        // REST-сервиса контейнером, клиент вообще не должен знать об этом
        // параметре. Соответственно, интерфейс клиента такого REST-сервиса не
        // может быть идентичен интерфейсу REST-сервиса.
        @GET
        String get();
    }
}
