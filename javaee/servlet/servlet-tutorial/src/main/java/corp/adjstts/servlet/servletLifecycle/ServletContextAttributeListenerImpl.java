package corp.adjstts.servlet.servletLifecycle;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

// https://docs.oracle.com/javaee/7/tutorial/servlets002.htm#BNAFI
@WebListener
public class ServletContextAttributeListenerImpl implements ServletContextAttributeListener {
    @Override
    public void attributeAdded(ServletContextAttributeEvent servletContextAttributeEvent) {
        servletContextAttributeEvent.getServletContext().log(
            "Servlet context attribute added: " + servletContextAttributeEvent.getName()
                + "=" + servletContextAttributeEvent.getValue());
    }

    @Override
    public void attributeRemoved(ServletContextAttributeEvent servletContextAttributeEvent) {
        servletContextAttributeEvent.getServletContext().log(
            "Servlet context attribute removed: " + servletContextAttributeEvent.getName()
                + "=" + servletContextAttributeEvent.getValue());
    }

    @Override
    public void attributeReplaced(ServletContextAttributeEvent servletContextAttributeEvent) {
        servletContextAttributeEvent.getServletContext().log(
            "Servlet context attribute replaced: " + servletContextAttributeEvent.getName());
    }
}

