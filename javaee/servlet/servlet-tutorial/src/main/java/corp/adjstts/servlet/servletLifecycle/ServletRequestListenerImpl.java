package corp.adjstts.servlet.servletLifecycle;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

// https://docs.oracle.com/javaee/7/tutorial/servlets002.htm#BNAFI
@WebListener
public class ServletRequestListenerImpl implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        getHttpRequest(servletRequestEvent).ifPresent(r ->
            servletRequestEvent.getServletContext().log("Http request destroyed for path " + r.getServletPath()));
    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        getHttpRequest(servletRequestEvent).ifPresent(r ->
            servletRequestEvent.getServletContext().log("Http request initialized for path " + r.getServletPath()));
    }

    private Optional<HttpServletRequest> getHttpRequest(ServletRequestEvent servletRequestEvent) {
        return Optional.of(servletRequestEvent)
            .map(ServletRequestEvent::getServletRequest)
            .filter(HttpServletRequest.class::isInstance)
            .map(HttpServletRequest.class::cast);
    }
}
