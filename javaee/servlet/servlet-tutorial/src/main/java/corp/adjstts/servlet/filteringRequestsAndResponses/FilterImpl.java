package corp.adjstts.servlet.filteringRequestsAndResponses;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.time.LocalTime;

// https://docs.oracle.com/javaee/7/tutorial/servlets006.htm#BNAGB
@WebFilter("/filtered/*")
public class FilterImpl implements Filter {
    @Override
    public void init(FilterConfig filterConfig) {
        // NOP
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        // фильтр для демонстрации: пропускает только запросы, выполненные в четную секунду.
        if (LocalTime.now().getSecond() % 2 == 0) {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
        // NOP
    }
}
