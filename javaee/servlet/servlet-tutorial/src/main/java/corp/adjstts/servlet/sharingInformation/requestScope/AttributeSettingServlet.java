package corp.adjstts.servlet.sharingInformation.requestScope;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// https://docs.oracle.com/javaee/7/tutorial/servlets003.htm#BNAFO
@WebServlet("/request/set-attribute")
public class AttributeSettingServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("my request attribute", "my request attribute value");
        // смысла конечно в этом мало тут, потому что запрос к AttrbuteSettingServlet существует отдельно от запроса к AttributeGettingServlet
    }
}
