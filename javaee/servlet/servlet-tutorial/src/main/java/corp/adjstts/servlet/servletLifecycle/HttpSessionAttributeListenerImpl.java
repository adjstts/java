package corp.adjstts.servlet.servletLifecycle;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

// https://docs.oracle.com/javaee/7/tutorial/servlets002.htm#BNAFI
@WebListener
public class HttpSessionAttributeListenerImpl implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent httpSessionBindingEvent) {
        httpSessionBindingEvent.getSession().getServletContext().log(
            "Http session (id = " + httpSessionBindingEvent.getSession().getId()
                + ") attribute added: " + httpSessionBindingEvent.getName()
                + "=" + httpSessionBindingEvent.getValue());
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {
        httpSessionBindingEvent.getSession().getServletContext().log(
            "Http session (id = " + httpSessionBindingEvent.getSession().getId()
                + ") attribute removed: " + httpSessionBindingEvent.getName());
    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {
        httpSessionBindingEvent.getSession().getServletContext().log(
            "Http session (id = " + httpSessionBindingEvent.getSession().getId()
                + ") attribute replaced: " + httpSessionBindingEvent.getName());
    }
}
