package corp.adjstts.servlet.sharingInformation.sessionScope;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// https://docs.oracle.com/javaee/7/tutorial/servlets003.htm#BNAFO
@WebServlet("/session/set-attribute")
public class AttributeSettingServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession().setAttribute("my session attribute", "my session attribute value");
    }
}
