package corp.adjstts.servlet.servletLifecycle;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

// https://docs.oracle.com/javaee/7/tutorial/servlets002.htm#BNAFI
@WebListener
public class HttpSessionListenerImpl implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().getServletContext().log(
            "Http session created with id " + httpSessionEvent.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().getServletContext().log(
            "Http session destroyed with id " + httpSessionEvent.getSession().getId());
    }
}
