package corp.adjstts.servlet.sharingInformation.webContextScope;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// https://docs.oracle.com/javaee/7/tutorial/servlets003.htm#BNAFO
@WebServlet("/web-context/get-attribute")
public class AttributeGettingServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try (PrintWriter writer = resp.getWriter()) {
            writer.println(req.getServletContext().getAttribute("my web context attribute"));
        }
    }
}
