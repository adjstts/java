package corp.adjstts.servlet.servletLifecycle;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.annotation.WebListener;

// https://docs.oracle.com/javaee/7/tutorial/servlets002.htm#BNAFI
@WebListener
public class ServletRequestAttributeListenerImpl implements ServletRequestAttributeListener {
    @Override
    public void attributeAdded(ServletRequestAttributeEvent servletRequestAttributeEvent) {
        servletRequestAttributeEvent.getServletContext().log("Servlet request attribute added "
            + servletRequestAttributeEvent.getName() + "=" + servletRequestAttributeEvent.getValue());
    }

    @Override
    public void attributeRemoved(ServletRequestAttributeEvent servletRequestAttributeEvent) {
        servletRequestAttributeEvent.getServletContext().log("Servlet request attribute removed: "
            + servletRequestAttributeEvent.getName());
    }

    @Override
    public void attributeReplaced(ServletRequestAttributeEvent servletRequestAttributeEvent) {
        servletRequestAttributeEvent.getServletContext().log("Servlet request attribute replaced: "
            + servletRequestAttributeEvent.getName());
    }
}
