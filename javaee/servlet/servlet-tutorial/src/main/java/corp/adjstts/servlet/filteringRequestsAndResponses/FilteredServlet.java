package corp.adjstts.servlet.filteringRequestsAndResponses;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// https://docs.oracle.com/javaee/7/tutorial/servlets006.htm#BNAGB
@WebServlet("/filtered/servlet")
public class FilteredServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        try (PrintWriter writer = resp.getWriter()) {
            writer.println("A response from a filtered servlet!");
        }
    }
}
