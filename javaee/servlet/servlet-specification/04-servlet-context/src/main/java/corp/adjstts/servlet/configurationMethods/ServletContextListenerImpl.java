package corp.adjstts.servlet.configurationMethods;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// https://javaee.github.io/servlet-spec/downloads/servlet-3.1/Final/servlet-3_1-final.pdf#G8.999475
@WebListener
public class ServletContextListenerImpl implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        // Логгируем факт вызова метода нашей реализации ServletContextListener
        sce.getServletContext().log("ServletContextListenerImpl#contextInitialized invoked!");

        // Используем возможность регистрировать сервлеты из реализации ServletContextListener
        ServletRegistration.Dynamic registration =
            sce.getServletContext().addServlet("context listener created servlet",
                new HttpServlet() {
                    @Override
                    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
                        try (PrintWriter writer = resp.getWriter()) {
                            writer.println("Response from a context listener created servlet!");
                        }
                    }
                });

        registration.addMapping("/context-listener-created-servlet");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // NOP
    }
}
