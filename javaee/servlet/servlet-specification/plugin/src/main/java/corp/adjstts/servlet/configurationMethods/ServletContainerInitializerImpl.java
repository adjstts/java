package corp.adjstts.servlet.configurationMethods;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;

// https://docs.oracle.com/javase/tutorial/ext/basics/spi.html
// https://javaee.github.io/servlet-spec/downloads/servlet-3.1/Final/servlet-3_1-final.pdf#G8.999475
// https://javaee.github.io/servlet-spec/downloads/servlet-3.1/Final/servlet-3_1-final.pdf#G12.1001739 (8.2.4 Shared libraries / runtimes pluggability)
/**
 * Этот класс -- Service Provider в терминах SPI.
 * В META-INF/services этого модуля этот Service Provider указан как реализация {@link ServletContainerInitializer},
 * который является Service Provider Interface.
 * Такой Service Provider мог находиться потенциально в любом jar, находящемся в classpath war-архива этого
 * модуля (см. https://docs.oracle.com/javase/tutorial/ext/basics/spi.htm). здесь для мпростоты, реализация SPI
 * находится в этом же архиве.
 */
public class ServletContainerInitializerImpl implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) {
        // Логгируем факт вызова метода нашей реализации ServletContainerInitializer
        servletContext.log("ServletContainerInitializerImpl#onStartup invoked!");

        // Используем возможность регистрировать сервлеты из реализации ServletContainerInitializer
        ServletRegistration.Dynamic registration =
            servletContext.addServlet("container initializer created servlet",
                new HttpServlet() {
                    @Override
                    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
                        try (PrintWriter writer = resp.getWriter()) {
                            writer.println("Response from a container initializer created servlet!");
                        }
                    }
                });

        registration.addMapping("/container-initializer-created-servlet");
    }
}
