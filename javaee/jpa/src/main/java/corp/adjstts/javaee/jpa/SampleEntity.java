package corp.adjstts.javaee.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

@Entity
// Java EE 7 Tutorial, 38.1.7.4 Using Queries
@NamedQuery(name="findAllSampleEntities", query = "select se from SampleEntity se")
// Java EE 7 Tutorial, 37.1.1 Requirements for Entity Classes
public class SampleEntity {
    /**
     * Java EE 7 Tutorial 37.1.3 Primary Keys in Entities:
     *   Every entity must have a primary key. An entity may have either a simple or a composite primary key.
     */
    @Id
    private Integer id;

    private String string;

    private Integer number;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }
}
