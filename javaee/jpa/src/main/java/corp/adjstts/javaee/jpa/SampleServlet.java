package corp.adjstts.javaee.jpa;


import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// Java EE 7 Tutorial, 17.1 What Is a Servlet?
// Java EE 7 Tutorial, 17.4 Creating and Initializing a Servlet
@WebServlet("/sample-servlet")
public class SampleServlet extends HttpServlet {
    @EJB
    private SampleEJB sampleEJB;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        SampleEntity sampleEntity = sampleEJB.getOneSampleEntity();

        try (PrintWriter writer = resp.getWriter()) {
            writer.println(SampleEntity.class.getSimpleName()
                + "(string=" + sampleEntity.getString() + ", number=" + sampleEntity.getNumber() + ")");
        }
    }
}
