package corp.adjstts.javaee.jpa;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class SampleEJB {
    @PersistenceContext(unitName = "SamplePersistenceUnit")
    private EntityManager entityManager;

    // методы EJB по умолчанию выполняются в транзакции (CMT)
    // публичные методы EJB составляют его local no-interface view
    public SampleEntity getOneSampleEntity() {
        // Запрос findAllSampleEntities определен аннотацией к сущности SampleEntity.
        return (SampleEntity) entityManager.createNamedQuery("findAllSampleEntities").getSingleResult();
    }
}
