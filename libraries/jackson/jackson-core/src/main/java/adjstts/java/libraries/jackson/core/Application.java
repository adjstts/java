package adjstts.java.libraries.jackson.core;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

/**
 * Пример создания и использования JsonGenerator. JsonGenerator создается через
 * JsonFactory. Пример создания и использования JsonParser должен выглядеть
 * аналогично этому.
 */
public class Application
{
    public static void main(String[] args) throws Exception
    {
        new Application().start();
    }

    private void start() throws Exception {
        JsonFactory factory = new JsonFactory();

        JsonGenerator generator = factory.createGenerator(System.out);

        Car car = new Car("Mustang", new Wheel[]{ new Wheel(5),
            new Wheel(5), new Wheel(5), new Wheel(5) });

        generator.writeObject(car);

        generator.writeStartObject();
        generator.writeStringField("my key", "my value");

        // так не получится, потому что jackson-core -- это stream api, и для
        // записи объекта нужен кодек.
        // generator.writeObjectField("my car", car);

        generator.writeEndObject();

        // без этого написанное так и останется в буфере и не будет выведено.
        generator.flush();  // {"my key":"my value"}
    }

    private class Car {
        private String name;
        private Wheel[] wheels;

        private Car(String name, Wheel[] wheels)
        {
            this.name = name;
            this.wheels = wheels;
        }
    }

    private class Wheel {
        private int size;

        private Wheel(int size)
        {
            this.size = size;
        }
    }
}
