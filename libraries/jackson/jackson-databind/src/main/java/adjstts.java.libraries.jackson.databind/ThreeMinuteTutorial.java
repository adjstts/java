package adjstts.java.libraries.jackson.databind;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.List;
import java.util.Map;

/**
 * 3 minute tutorial: Generic collections, Tree Model
 * https://github.com/FasterXML/jackson-databind#3-minute-tutorial-generic-collections-tree-model
 */
public class ThreeMinuteTutorial
{
    private ObjectMapper mapper;

    public static void main(String[] args) throws Exception {
        ThreeMinuteTutorial tutorial = new ThreeMinuteTutorial();
        tutorial.mapper = new ObjectMapper();

        // Beyond dealing with simple Bean-style POJOs, you can also handle JDK
        // Lists, Maps:
        tutorial.readMap();
        tutorial.readList();
        // as long as JSON structure matches, and types are simple.

        // If you have POJO values, you need to indicate actual type (note: this
        // is NOT needed for POJO properties with List etc types):
        tutorial.readPojoValuedMap();

        // While dealing with Maps, Lists and other "simple" Object types
        // (Strings, Numbers, Booleans) can be simple, Object traversal can be
        // cumbersome. This is where Jackson's Tree model can come in handy:
        tutorial.treeModel();
    }

    private void readMap() throws Exception {
        String jsonSource = "{\"a\":1,\"b\":2}";

        Map<String, Integer> numbers = mapper.readValue(jsonSource, Map.class);

        System.out.println(numbers); // {a=1, b=2}
    }

    private void readList() throws Exception {
        String jsonSource = "[5, 4, 3, 2, 1]";

        List<Integer> numbers = mapper.readValue(jsonSource, List.class);

        System.out.println(numbers); // [5, 4, 3, 2, 1]
    }

    private void readPojoValuedMap() throws Exception {
        String jsonSource = "{\"pojo\":{\"name\":\"sample pojo\"}}";

        Map<String, Pojo> pojos = mapper.readValue(jsonSource,
            new TypeReference<Map<String, Pojo>>(){});
        // why extra work? Java Type Erasure will prevent type detection
        // otherwise

        System.out.println(pojos);
        // {pojo=adjstts.java.libraries.jackson.databind
        //     .ThreeMinuteTutorial$Pojo@7f9a81e8}
    }

    private void treeModel() throws Exception {
        String jsonSource = "{\"name\":\"Bob\",\"age\":13}";

        // can be read as generic JsonNode, if it can be Object or Array; or,
        // if known to be Object, as ObjectNode, if array, ArrayNode etc:
        ObjectNode root = (ObjectNode) mapper.readTree(jsonSource);

        System.out.println(root.get("name").asText()); // Bob
        System.out.println(root.get("age").asText()); // 13

        // can modify as well: this adds child Object as property 'other', set
        // property 'type'
        root.with("other").put("type", "student");

        System.out.println(mapper.writeValueAsString(root));
        // {"name":"Bob","age":13,"other":{"type":"student"}}

        // Tree Model can be more convenient than data-binding, especially in
        // cases where structure is highly dynamic, or does not map nicely to
        // Java classes.
    }

    // Note: can use getters/setters as well; here we just use public fields
    // directly:
    static class Pojo {
        public String name;
        // NOTE: if using getters/setters, can keep fields `protected` or
        // `private`
    }
}
