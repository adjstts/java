package adjstts.java.libraries.jackson.databind;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 * 1 minute tutorial: POJOs to JSON and back
 * https://github.com/FasterXML/jackson-databind#1-minute-tutorial-pojos-to-json-and-back
 */
public class OneMinuteTutorial
{
    private ObjectMapper mapper;

    public static void main(String[] args) throws Exception {
        OneMinuteTutorial tutorial = new OneMinuteTutorial();
        tutorial.mapper = new ObjectMapper();
        // The default instance is fine for our use -- we will learn later on
        // how to configure mapper instance if necessary.

        tutorial.read();
        tutorial.write();
    }

    private void read() throws IOException {
        // MyValue value = mapper.readValue(new File("data.json"),
        //     MyValue.class);
        // or:
        // MyValue value = mapper.readValue(
        //     new URL("http://some.com/api/entry.json"), MyValue.class);
        // or:
        MyValue value = mapper.readValue("{\"name\":\"Bob\", \"age\":13}",
            MyValue.class);
    }

    private void write() throws IOException {
        MyValue value = new MyValue();
        value.name = "Bob";
        value.age = 13;

        String jsonString = mapper.writeValueAsString(value);

        System.out.println(jsonString);
    }

    // Note: can use getters/setters as well; here we just use public fields
    // directly:
    static class MyValue {
        public String name;
        public int age;
        // NOTE: if using getters/setters, can keep fields `protected` or
        // `private`
    }
}
