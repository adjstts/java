package adjstts.java.libraries.jackson.databind;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.StringWriter;

/**
 * 5 minute tutorial: Streaming parser, generator
 * https://github.com/FasterXML/jackson-databind#5-minute-tutorial-streaming-parser-generator
 *
 * As convenient as data-binding (to/from POJOs) can be; and as flexible as
 * Tree model can be, there is one more canonical processing model available:
 * incremental (aka "streaming") model. It is the underlying processing model
 * that data-binding and Tree Model both build upon, but it is also exposed to
 * users who want ultimate performance and/or control over parsing or generation
 * details.
 *
 * For in-depth explanation, look at Jackson Core component.
 */
public class FiveMinuteTutorial
{
    private ObjectMapper mapper;

    public static void main(String[] args) throws Exception {
        FiveMinuteTutorial tutorial = new FiveMinuteTutorial();
        tutorial.mapper = new ObjectMapper();

        tutorial.incrementalModelGenerate();
        tutorial.incrementalModelParse();
    }

    private void incrementalModelGenerate() throws Exception {
        JsonFactory factory = mapper.getFactory();

        StringWriter out = new StringWriter();

        // write JSON: { "message" : "Hello world!" }
        JsonGenerator generator = factory.createGenerator(out);
        generator.writeStartObject();
        generator.writeObjectField("message", "Hello World!");
        generator.writeEndObject();
        generator.close();

        System.out.println(out.toString()); // {"message":"Hello World!"}
    }

    private void incrementalModelParse() throws Exception {
        JsonFactory factory = mapper.getFactory();

        String jsonSource = "{\"message\":\"Hello World!\"}";

        JsonParser parser = factory.createParser(jsonSource);

        JsonToken token = parser.nextToken(); // JsonToken.START_OBJECT

        token = parser.nextToken(); // JsonToken.FIELD_NAME
        System.out.println(parser.getCurrentName()); // message

        token = parser.nextToken(); // JsonToken.VALUE_STRING
        System.out.println(parser.getText());  // Hello World!

        parser.close();
    }
}
