package adjstts.java.libraries.jackson.databind;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * 10 minute tutorial: configuration
 * https://github.com/FasterXML/jackson-databind#10-minute-tutorial-configuration
 *
 * There are two entry-level configuration mechanisms you are likely to use:
 * Features and Annotations.
 */
public class TenMinuteTutorial
{
    private ObjectMapper mapper;

    public static void main(String[] args)
    {
        TenMinuteTutorial tutorial = new TenMinuteTutorial();
        tutorial.mapper = new ObjectMapper();

        tutorial.features();
    }

    private void features() {
        // Let's start with higher-level data-binding configuration.

        // SerializationFeature for changing how JSON is written
        // to enable standard indentation ("pretty-printing"):
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        // to allow serialization of "empty" POJOs (no properties to serialize)
        // (without this setting, an exception is thrown in those cases)
        mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        // to write java.util.Date, Calendar as number (timestamp):
        mapper.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        // DeserializationFeature for changing how JSON is read as POJOs:
        // to prevent exception when encountering unknown property:
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        // to allow coercion of JSON empty String ("") to null Object value:
        mapper.enable(DeserializationFeature
            .ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        // In addition, you may need to change some of low-level JSON parsing,
        // generation details:

        // JsonParser.Feature for configuring parsing settings:
        // to allow C/C++ style comments in JSON (non-standard, disabled by
        // default)
        // (note: with Jackson 2.5, there is also `mapper.enable(feature)` /
        // `mapper.disable(feature)`)
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        // to allow (non-standard) unquoted field names in JSON:
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        // to allow use of apostrophes (single quotes), non standard
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        // JsonGenerator.Feature for configuring low-level JSON generation:
        // to force escaping of non-ASCII characters:
        mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);

        // Full set of features are explained on Jackson Features page.
    }

    // Annotations: changing property names

    // The simplest annotation-based approach is to use @JsonProperty annotation
    // like so:
    public class MyBean {
        private String _name;

        // without annotation, we'd get "theName", but we want "name":
        @JsonProperty("name")
        public String getTheName()
        {
            return _name;
        }

        // note: it is enough to add annotation on just getter OR setter;
        // so we can omit it here
        public void setTheName(String _name)
        {
            this._name = _name;
        }
    }
    // There are other mechanisms to use for systematic naming changes:
    // see JacksonCustomNamingConvention for details.
    // Note, too, that you can use JacksonMixinAnnotations to associate all
    // annotations.

    // Annotations: Ignoring properties

    // There are two main annotations that can be used to to ignore properties:
    // @JsonIgnore for individual properties; and @JsonIgnoreProperties for
    // per-class definition

    // means that if we see "foo" or "bar" in JSON, they will be quietly skipped
    // regardless of whether POJO has such properties
    @JsonIgnoreProperties
    public class MyBeanWithIgnoredProperties {
        // will not be written as JSON; nor assigned from JSON:
        @JsonIgnore
        public String internal;

        // no annotation, public field is read/written normally
        public String external;

        private int _code;

        @JsonIgnore
        public void setCode(int _code)
        {
            this._code = _code;
        }

        // note: will also be ignored because setter has annotation!
        public int getCode()
        {
            return _code;
        }
    }

    // As with renaming, note that annotations are "shared" between matching
    // fields, getters and setters: if only one has @JsonIgnore, it affects
    // others. But it is also possible to use "split" annotations, to for
    // example:
    public class ReadButDontWriteProps {
        private String _name;

        @JsonProperty
        public void setName(String _name)
        {
            this._name = _name;
        }

        @JsonIgnore
        public String getName()
        {
            return _name;
        }
    }
    // in this case, no "name" property would be written out (since 'getter' is
    // ignored); but if "name" property was found from JSON, it would be
    // assigned to POJO property! For a more complete explanation of all
    // possible ways of ignoring properties when writing out JSON, check
    // "Filtering properties" article.

    // Annotations: using custom constructor

    // Unlike many other data-binding packages, Jackson does not require you to
    // define "default constructor" (constructor that does not take arguments).
    // While it will use one if nothing else is available, you can easily define
    // that an argument-taking constructor is used:
    public class CtorBean {
        public final String name;
        public final int age;

        @JsonCreator // constructor can be public, private, whatever
        public CtorBean(@JsonProperty("name") String name,
            @JsonProperty("age") int age)
        {
            this.name = name;
            this.age = age;
        }
    }
    // Constructors are especially useful in supporting use of Immutable
    // objects.

    // Alternatively, you can also define "factory methods":
    public static class FactoryBean {
        // fields etc omitted for brewity

        public static FactoryBean create(@JsonProperty("name") String name) {
            // construct and return an instance
            return new FactoryBean();
        }
    }
    // Note that use of a "creator method" (@JsonCreator with @JsonProperty
    // annotated arguments) does not preclude use of setters: you can mix and
    // match properties from constructor/factory method with ones that are set
    // via setters or directly using fields.
}
