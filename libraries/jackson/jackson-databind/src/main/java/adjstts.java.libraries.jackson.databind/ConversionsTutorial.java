package adjstts.java.libraries.jackson.databind;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Tutorial: fancier stuff, conversions
 * https://github.com/FasterXML/jackson-databind#tutorial-fancier-stuff-conversions
 *
 * One useful (but not very widely known) feature of Jackson is its ability to
 * do arbitrary POJO-to-POJO conversions. Conceptually you can think of
 * conversions as sequence of 2 steps: first, writing a POJO as JSON, and
 * second, binding that JSON into another kind of POJO. Implementation just
 * skips actual generation of JSON, and uses more efficient intermediate
 * representation.
 */
public class ConversionsTutorial
{
    private ObjectMapper mapper;

    public static void main(String[] args)
    {
        ConversionsTutorial tutorial = new ConversionsTutorial();
        tutorial.mapper = new ObjectMapper();

        // Conversions work between any compatible types, and invocation is as
        // simple as:
        //
        // ResultType result = mapper.convertValue(sourceObject, ResultType.class);
        //
        // and as long as source and result types are compatible -- that is, if
        // to-JSON, from-JSON sequence would succeed -- things will "just work".

        // But here are couple of potentially useful use cases:
        tutorial.convertIntegerListToArray();
        tutorial.convertPojoToMap();
        tutorial.convertMapToPojo();
        tutorial.decodeBase64();
    }

    private void convertIntegerListToArray() {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        int[] array = mapper.convertValue(list, int[].class);

        for (int i : array)
        {
            System.out.println(i);
        }
    }

    private void convertPojoToMap() {
        PojoType pojoValue = new PojoType();
        pojoValue.setName("some name");
        pojoValue.setAge(-1);

        Map<String, Object> map = mapper.convertValue(pojoValue, Map.class);

        System.out.println(map);
    }

    private void convertMapToPojo() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "some name");
        map.put("age", -1);

        PojoType pojoValue = mapper.convertValue(map, PojoType.class);

        System.out.println(String.format("PojoType(name=%s, age=%d)",
            pojoValue.getName(), pojoValue.getAge()));
    }

    private void decodeBase64() {
        String base64 = "TWFuIGlzIGRpc3Rpbmd1aXNoZWQsIG5vdCBvbmx5IGJ5IGhpcyByZ"
            + "WFzb24sIGJ1dCBieSB0aGlz";

        byte[] binary = mapper.convertValue(base64, byte[].class);

        for (byte b : binary) {
            System.out.print(b);
        }
    }

    static class PojoType {
        private String name;
        private int age;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public int getAge()
        {
            return age;
        }

        public void setAge(int age)
        {
            this.age = age;
        }
    }
}
